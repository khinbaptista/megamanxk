﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Actors
{
    public enum Action
    {
        PreSpawn,
        Spawn,
        Idle,
        Walking,
        Jumping,
        Falling,
        Shooting,
        WalkingShooting,
        JumpShoot,
        FallShoot,
        Hit,
        Dying,
        Victory
    }

    abstract class Actor
    {
        protected enum ANINDEX      // animation index
        {
            MoveRight = 0,
            Jump,
            Fall,
            Shoot,
            Idle,
            Hit,
            Die,
            Spawn,
            Victory,
            WalkShoot,
            JumpShoot,
            FallShoot
        }

        protected GameScreen levelScreen;   // the level where this actor is.
        protected Rectangle position;
        protected Rectangle frame;

        protected Texture2D spriteSheet;
        protected List<Animation> animations;
        protected int animFrameIndex;
        protected const float frameInterval = 70.0f;
        protected float timer;

        protected Action action;
        protected Action previousAction;
        protected int speed;

        public int healthPoints { get; protected set; } // 110 246 20 20 x 4  = projectile source rectangle
        protected int damageTaken = 0;

        public Actor(GameScreen level)
        {
            //action = Action.PreSpawn;
            animations = new List<Animation>();
            this.levelScreen = level;

            timer = 0.0f;
            animFrameIndex = 0;
        }

        public void ChangeAction(Action newAction, int newFrameIndex = 0)
        {
            previousAction = action;
            action = newAction;
            animFrameIndex = newFrameIndex;     // new animations play from the beggining
        }

        public virtual void Update(GameTime gameTime) { }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) { }

        public Rectangle getPosition() { return position; }

        public void setPosition(int x, int y)
        {
            position.X = x;
            position.Y = y;
        }

        public void gotHit(int damage)
        {
            if (action == Action.Hit)
                return;

            damageTaken = damage;
            ChangeAction(Action.Hit);

        }

        protected void Hurt()
        {
            healthPoints -= damageTaken;

            // debug
            Console.WriteLine("HP: " + healthPoints);
        }

        public Action getAction() { return action; }

        public int getFrame() { return animFrameIndex; }
    }
}
