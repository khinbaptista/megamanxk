﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Actors
{
    class Character : Actor
    {
        #region ATTRIBUTES

        private SpriteEffects effect;

        private const int spawnLazerSpeed = 20;
        private const int jumpSpeed = 15;
        private const int maxJumpOffset = 300;
        private int jumpOffset = 0;

        private bool dead = false;
        private bool victorious = false;

        private float shootTimer = 0;
        private const float shootInterval = 200;
        private bool shootTrigger = false;

        private float hitTimer = 0;
        private const float hitInterval = 400;

        private SoundEffect dieSFX;
        private SoundEffect hitSFX;
        private SoundEffect spawnSFX;
        private bool playSpawnSFX = true;

        #endregion

        #region METHODS

        public Character(GameScreen level)
            : base(level)
        {
            spriteSheet = MegaMain.getContentManager().Load<Texture2D>("megamanSheet02");
            action = Action.PreSpawn;
            previousAction = action;

            frame = new Rectangle();
            position = new Rectangle(100, 0, 80, 120);
            effect = SpriteEffects.None;
            speed = 6;
            healthPoints = 20;

            dieSFX = MegaMain.getContentManager().Load<SoundEffect>(@"Sounds/die");
            hitSFX = MegaMain.getContentManager().Load<SoundEffect>(@"Sounds/charHit");
            spawnSFX = MegaMain.getContentManager().Load<SoundEffect>(@"Sounds/charSpawn");

            SetupAnimations();
        }

        private void SetupAnimations()
        {
            Animation animation;
            int frameWidth = 40;
            int frameHeight = 60;

            // ANINDEX.MoveRight
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 3, 11);
            animations.Add(animation);

            // ANINDEX.Jump
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 4, 4);
            animations.Add(animation);

            // ANINDEX.Fall
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 4, 3, 4);
            animations.Add(animation);

            // ANINDEX.Shoot
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 2, 3);
            animations.Add(animation);

            // ANINDEX.Idle
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 1, 5);
            animations.Add(animation);

            // ANINDEX.Hit
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 6, 9);
            animations.Add(animation);

            // ANINDEX.Die
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 7, 4);
            animations.Add(animation);

            // ANINDEX.Spawn
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 0, 8);
            animations.Add(animation);

            // ANINDEX.Victory
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 5, 10);
            animations.Add(animation);

            // ANINDEX.WalkShoot
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 8, 10);
            animations.Add(animation);

            // ANINDEX.JumpShoot
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 9, 4);
            animations.Add(animation);

            // ANINDEX.FallShoot
            animation = new Animation();
            animation.getAnimationFromNormalized(frameWidth, frameHeight, 9, 3, 4);
            animations.Add(animation);

        }

        public override void Update(GameTime gameTime)
        {
            timer += gameTime.ElapsedGameTime.Milliseconds;

            if (timer > frameInterval)
            {
                animFrameIndex++;
                timer = 0;
            }

            switch (action)
            {
                case Action.PreSpawn:
                    if (playSpawnSFX)
                    {
                        spawnSFX.Play();
                        playSpawnSFX = false;
                    }
                    animFrameIndex = 0;
                    frame = animations[(int)ANINDEX.Spawn].getFrame(animFrameIndex);
                    position.Y += spawnLazerSpeed;
                    if (position.Intersects(levelScreen.GetSpawnCollider()))
                        ChangeAction(Action.Spawn, 1);
                    break;

                case Action.Spawn:
                    if (animFrameIndex == animations[(int)ANINDEX.Spawn].getFrameCount())
                        ChangeAction(Action.Idle);
                    else
                        frame = animations[(int)ANINDEX.Spawn].getFrame(animFrameIndex);
                    break;

                case Action.Idle:
                    if (animFrameIndex == animations[(int)ANINDEX.Idle].getFrameCount())
                        ResetFrameIndex();
                    frame = animations[(int)ANINDEX.Idle].getFrame(animFrameIndex);
                    break;

                case Action.Walking:
                    if (animFrameIndex == animations[(int)ANINDEX.MoveRight].getFrameCount())
                        ResetFrameIndex();
                    frame = animations[(int)ANINDEX.MoveRight].getFrame(animFrameIndex);
                    Walk();
                    break;

                case Action.Jumping:
                    if (animFrameIndex == animations[(int)ANINDEX.Jump].getFrameCount())
                        ResetFrameIndex(animFrameIndex - 1);
                    frame = animations[(int)ANINDEX.Jump].getFrame(animFrameIndex);
                    break;

                case Action.Falling:
                    if (animFrameIndex == animations[(int)ANINDEX.Fall].getFrameCount())
                        ChangeAction(Action.Idle);
                    else frame = animations[(int)ANINDEX.Fall].getFrame(animFrameIndex);
                    break;

                case Action.Shooting:
                    ControlShoot(gameTime);
                    if (animFrameIndex == animations[(int)ANINDEX.Shoot].getFrameCount())
                    {
                        ChangeAction(Action.Idle);
                        break;
                    }
                    frame = animations[(int)ANINDEX.Shoot].getFrame(animFrameIndex);
                    break;

                case Action.WalkingShooting:
                    ControlShoot(gameTime);
                    if (animFrameIndex == animations[(int)ANINDEX.WalkShoot].getFrameCount())
                        ChangeAction(Action.Idle);
                    else
                        frame = animations[(int)ANINDEX.WalkShoot].getFrame(animFrameIndex);
                    break;

                case Action.JumpShoot:
                    ControlShoot(gameTime);
                    if (animFrameIndex == animations[(int)ANINDEX.JumpShoot].getFrameCount())
                        ResetFrameIndex(animFrameIndex - 1);
                    frame = animations[(int)ANINDEX.JumpShoot].getFrame(animFrameIndex);
                    break;

                case Action.FallShoot:
                    ControlShoot(gameTime);
                    if (animFrameIndex == animations[(int)ANINDEX.FallShoot].getFrameCount())
                        ChangeAction(Action.Idle);
                    else
                        frame = animations[(int)ANINDEX.FallShoot].getFrame(animFrameIndex);
                    break;

                case Action.Hit:
                    SlideOnHit();
                    ControlHit(gameTime);
                    if (animFrameIndex == 0)
                        hitSFX.Play();
                    if (animFrameIndex == animations[(int)ANINDEX.Hit].getFrameCount())
                        ChangeAction(healthPoints <= 0? Action.Dying : Action.Idle);
                    else
                        frame = animations[(int)ANINDEX.Hit].getFrame(animFrameIndex);
                    break;

                case Action.Dying:
                    if (animFrameIndex == 0)
                        dieSFX.Play();
                    if (animFrameIndex == animations[(int)ANINDEX.Die].getFrameCount())
                    {
                        ResetFrameIndex(animFrameIndex - 1);
                        if (!dead)
                        {
                            System.Threading.Thread.Sleep(200);
                            levelScreen.ManDown();
                        }
                        dead = true;
                    }
                    frame = animations[(int)ANINDEX.Die].getFrame(animFrameIndex);
                    break;

                case Action.Victory:
                    if (animFrameIndex == animations[(int)ANINDEX.Victory].getFrameCount())
                    {
                        ResetFrameIndex(animFrameIndex - 1);
                        if (!victorious)
                        {
                            System.Threading.Thread.Sleep(200);
                            levelScreen.Victory();
                        }
                        victorious = true;
                    }
                    frame = animations[(int)ANINDEX.Victory].getFrame(animFrameIndex);
                    break;

                default:
                    break;
            }
        }

        private void SlideOnHit()
        {
            if (effect == SpriteEffects.None)
                position.X -= 1;
            else if (effect == SpriteEffects.FlipHorizontally)
                position.X += 1;
        }

        private void ControlShoot(GameTime gameTime)
        {
            shootTimer += gameTime.ElapsedGameTime.Milliseconds;

            if (shootTimer > shootInterval)
                shootTrigger = true;

            Shoot();
        }

        private void ControlHit(GameTime gameTime)
        {
            hitTimer += gameTime.ElapsedGameTime.Milliseconds;

            if (hitTimer > hitInterval)
            {
                hitTimer = 0;
                Hurt();
            }

        }

        public void Shoot()
        {
            Rectangle lemonPosition = new Rectangle();

            if (!shootTrigger)
                return;

            shootTimer = 0;
            shootTrigger = false;

            if (effect == SpriteEffects.None)
                lemonPosition = new Rectangle(position.Right, position.Y + 50, 60, 60);
            else if (effect == SpriteEffects.FlipHorizontally)
                lemonPosition = new Rectangle(position.X - 60, position.Y + 50, 60, 60);

            levelScreen.NewProjectile(new Projectile(lemonPosition, effect));
        }

        public void HandleInput(InputHandler input)
        {
            switch (action)
            {
                case Action.Idle:
                    if (input.status.Contains(Input.Left))
                    {
                        if (effect == SpriteEffects.FlipHorizontally)
                            ChangeAction(Action.Walking);
                        else if (effect == SpriteEffects.None)
                            FaceLeft();
                    }
                    else if (input.status.Contains(Input.Right))
                    {
                        if (effect == SpriteEffects.None)
                            ChangeAction(Action.Walking);
                        else if (effect == SpriteEffects.FlipHorizontally)
                            FaceRight();
                    }
                    else if (input.status.Contains(Input.A))
                    {
                        jumpOffset = jumpSpeed;
                        ChangeAction(Action.Jumping);
                    }
                    else if (input.status.Contains(Input.X))
                        if (!input.previousStatus.Contains(Input.X))
                            ChangeAction(Action.Shooting);
                    break;

                case Action.Walking:
                    if (!(input.status.Contains(Input.Left) || input.status.Contains(Input.Right)))
                        ChangeAction(Action.Idle);
                    else if (input.status.Contains(Input.A))
                    {
                        jumpOffset = jumpSpeed;
                        ChangeAction(Action.Jumping);
                    }
                    else if (input.status.Contains(Input.X) && (input.status.Contains(Input.Left) || input.status.Contains(Input.Right)))
                        if (!input.previousStatus.Contains(Input.X))
                            ChangeAction(Action.WalkingShooting);
                    break;

                case Action.Jumping:
                    if (input.status.Contains(Input.A) && jumpOffset != 0)
                    {
                        jumpOffset += jumpSpeed;
                        position.Y -= jumpSpeed;

                        if (jumpOffset > maxJumpOffset)
                            jumpOffset = 0;
                    }
                    else if (input.status.Contains(Input.X))
                        ChangeAction(Action.JumpShoot, animFrameIndex);
                    else jumpOffset = 0;
                    InputWalk(input);
                    break;

                case Action.Falling:
                    InputWalk(input);
                    if (input.status.Contains(Input.X))
                        ChangeAction(Action.FallShoot, animFrameIndex);
                    break;

                case Action.WalkingShooting:
                    if (!input.status.Contains(Input.X))
                        ChangeAction(Action.Idle);
                    InputWalk(input);
                    if (input.status.Contains(Input.A))
                    {
                        jumpOffset = jumpSpeed;
                        ChangeAction(Action.JumpShoot);
                    }
                    break;

                case Action.JumpShoot:
                    if (!input.status.Contains(Input.X))
                        ChangeAction(Action.Jumping, animFrameIndex);
                    if (input.status.Contains(Input.A) && jumpOffset != 0)
                    {
                        jumpOffset += jumpSpeed;
                        position.Y -= jumpSpeed;

                        if (jumpOffset > maxJumpOffset)
                            jumpOffset = 0;
                    }
                    else
                        jumpOffset = 0;
                    InputWalk(input);
                    break;

                case Action.FallShoot:
                    InputWalk(input);
                    break;

                default:
                    break;
            }


        }

        private bool InputWalk(InputHandler input)
        {
            if (input.status.Contains(Input.Left))
            {
                if (effect == SpriteEffects.FlipHorizontally)
                    WalkLeft();
                else
                    FaceLeft();
            }
            else if (input.status.Contains(Input.Right))
            {
                if (effect == SpriteEffects.None)
                    WalkRight();
                else
                    FaceRight();
            }
            else return false;
            
            return true;
        }

        private void FaceLeft() { effect = SpriteEffects.FlipHorizontally; }

        private void FaceRight() { effect = SpriteEffects.None; }

        private void Walk()
        {
            if (effect == SpriteEffects.None)
                WalkRight();
            else if (effect == SpriteEffects.FlipHorizontally)
                WalkLeft();
        }

        private void WalkRight()
        {
            if (effect != SpriteEffects.None)
                return;

            if (position.X >= levelScreen.GetViewportRectangle().Width / 2)// - 600)
            {
                if (!levelScreen.SlideX(speed / 3))
                    if (position.X <= levelScreen.GetViewportRectangle().Width - position.Width)
                        position.X += speed;
            }
            else
                position.X += speed;
        }

        private void WalkLeft()
        {
            if (effect != SpriteEffects.FlipHorizontally)
                return;

            if (position.X <= 300)
            {
                if (!levelScreen.SlideX(-speed / 3))
                    if (position.X >= speed)
                        position.X -= speed;
            }
            else
                position.X -= speed;
        }

        private void ResetFrameIndex(int index = 0)
        {
            animFrameIndex = index;
            timer = 0;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(spriteSheet, position, frame, Color.White, 0.0f, new Vector2(), effect, 0);
            spriteBatch.End();
        }

        public Rectangle GetPosition() { return position; }

        public bool isPreSpawning() { return action == Action.PreSpawn; }

        public int getHP() { return healthPoints; }

        #endregion
    }
}
