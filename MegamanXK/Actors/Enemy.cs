﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Actors
{
    class Enemy : Actor
    {
        bool dead = false;
        int damage = 2;
        bool flip = true;
        bool awake = false;
        bool immovable;

        public Enemy(GameScreen level, Vector2 position, bool immovable = false, bool isFlipped = true)
            : base(level)
        {
            // square size for this enemy: 37 (3 frames, no effects, just movement and collision)
            action = Action.Idle;
            animations = new List<Animation>();
            healthPoints = 6;
            speed = 4;
            flip = isFlipped;
            this.immovable = immovable;

            this.position = new Rectangle((int)position.X, (int)position.Y, 74, 74); // 111

            spriteSheet = MegaMain.getContentManager().Load<Texture2D>("enemy02");

            Animation animation = new Animation();
            animation.getAnimationFromNormalized(37, 37, 0, 3);
            animations.Add(animation);
        }

        public override void Update(GameTime gameTime)
        {
            timer += gameTime.ElapsedGameTime.Milliseconds;

            if (timer > frameInterval)
            {
                animFrameIndex++;
                timer = 0;
            }

            if (animFrameIndex == 3)
                animFrameIndex = 0;
            
            if (flip)
                frame = animations[0].getFrame(2 - animFrameIndex);
            else
                frame = animations[0].getFrame(animFrameIndex);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(spriteSheet, position, frame, Color.White);
            spriteBatch.End();
        }

        public void GotHit(int damage)
        {
            healthPoints -= damage;

            if (healthPoints <= 0)
                dead = true;
        }

        public void Move()
        {
            if (immovable)
                return;

            if (flip)
                position.X += speed;
            else
                position.X -= speed;
        }

        public int getDamage() { return damage; }

        public bool isDead() { return dead; }

        public bool isFlipped() { return flip; }

        public void setFlip(bool value) { flip = value; }

        public void toggleFlip() { flip = !flip; }

        public void Wakeup() { awake = true; }

        public bool isAwake() { return awake; }
    }
}
