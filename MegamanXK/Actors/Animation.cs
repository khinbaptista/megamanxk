﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Actors
{
    public class Animation
    {
        private List<Rectangle> frames;

        public Animation()
        {
            frames = new List<Rectangle>();
        }

        /// <summary>
        /// Insert a new frame to the animation.
        /// </summary>
        /// <param name="rect">The clipping rectangle over the sprite sheet.</param>
        public void insertFrame(Rectangle rect) { frames.Add(rect); }

        /// <summary>
        /// Sets an animation.
        /// </summary>
        /// <param name="frameList"></param>
        public void insertFrameList(List<Rectangle> frameList) { frames = frameList; }

        /// <summary>
        /// Gets a frame of the animation.
        /// </summary>
        /// <param name="frameIndex">Index of the desired frame</param>
        /// <returns></returns>
        public Rectangle getFrame(int frameIndex) { return frames[frameIndex]; }

        /// <summary>
        /// Sets up animation where every frame has the same size and are located adjacently.
        /// </summary>
        /// <param name="frameWidth">Width of the frames</param>
        /// <param name="frameHeight">Height of the frames</param>
        /// <param name="line">Line to start looking (0 based)</param>
        /// <param name="frameCounter">Number of frames contained in the animation</param>
        /// <param name="column">If neccessary, column to start lookin (0 based)</param>
        /// <returns></returns>
        public void getAnimationFromNormalized(int frameWidth, int frameHeight, int line, int frameCounter, int column = 0)
        {
            for (int i = 0; i < frameCounter; i++)
                frames.Add(new Rectangle(column * frameWidth + frameWidth * i, line * frameHeight, frameWidth, frameHeight));
        }

        /// <summary>
        /// Gets the number of frames in this animation
        /// </summary>
        /// <returns></returns>
        public int getFrameCount() { return frames.Count; }
    }
}
