﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Actors
{
    public class Projectile
    {
        Rectangle source;
        Rectangle position;

        Texture2D sheet;
        SpriteEffects effect;

        List<Rectangle> animation;

        public int speed { get; private set; }
        private int frame = 0;
        private float frameTimer = 0;
        private const int frameInterval = 50;
        private readonly int damage = 2;

        private SoundEffect newSFX;

        /// <summary>
        /// Creates a new projectile.
        /// </summary>
        /// <param name="position">Position where the projectile is to appear</param>
        /// <param name="effect">Same SpriteEffect as megaman</param>
        public Projectile(Rectangle position, SpriteEffects effect)
        {
            source = new Rectangle();//110, 246, 20, 20);
            this.position = position;
            this.effect = effect;
            speed = 10;

            sheet = MegaMain.getContentManager().Load<Texture2D>("shots and items");

            animation = new List<Rectangle>();

            for (int i = 0; i < 4; i++)
                animation.Add(new Rectangle(110 + 20 * i, 246, 20, 20));

            newSFX = MegaMain.getContentManager().Load<SoundEffect>(@"Sounds/shoot");

            newSFX.Play();
        }

        public void Offset(int x, int y) { position.Offset(x, y); }

        public void SetPosition(int x, int y)
        {
            position.X = x;
            position.Y = y;
        }

        public void UpdatePosition()
        {
            //position.X = position.X + (effect == SpriteEffects.None ? speed : -speed);

            if (effect == SpriteEffects.None)
                position.X += speed;
            else if (effect == SpriteEffects.FlipHorizontally)
                position.X -= speed;
        }

        public Rectangle GetRectangle() { return position; }

        public void Update(GameTime gameTime)
        {
            frameTimer += gameTime.ElapsedGameTime.Milliseconds;

            if (frameTimer >= frameInterval)
            {
                frameTimer = 0;
                frame++;

                if (frame == 4)
                    frame = 0;
            }

            source = animation[frame];
        }

        public void Draw(GameTime gameTime, SpriteBatch batch, bool begun)
        {
            if (!begun)
                batch.Begin();
            
            batch.Draw(sheet, position, source, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 0);

            if (!begun)
                batch.End();
        }

        public int GetDamage() { return damage; }
    }
}
