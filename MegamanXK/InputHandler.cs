﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Kinect;


namespace MegamanXK
{
    public enum Input
    {
        Nothing,
        A,      // confirm or jump
        B,      // cancel
        Up,
        Down,
        Left,
        Right,
        X,      // attack
        Y,      // nothing... maybe toggle kinect use
        Start,  
        Back
    }

    public class InputHandler
    {
        public List<Input> previousStatus { get; private set; }
        public List<Input> status { get; private set; }

        public InputHandler()
        {
            status = new List<Input>();
            previousStatus = new List<Input>();
        }

        public void Update()
        {
            KeyboardState keyState = Keyboard.GetState();
            GamePadState padState = GamePad.GetState(Microsoft.Xna.Framework.PlayerIndex.One);

            previousStatus = new List<Input>();
            foreach (Input i in status)
                previousStatus.Add(i);
            status = new List<Input>();

            if (keyState.IsKeyDown(Keys.Escape) || padState.Buttons.B == ButtonState.Pressed)
                status.Add(Input.B);
            if (keyState.IsKeyDown(Keys.Space) || padState.Buttons.A == ButtonState.Pressed)
                status.Add(Input.A);
            if (keyState.IsKeyDown(Keys.Up) || padState.DPad.Up == ButtonState.Pressed || padState.ThumbSticks.Left.Y == 1)
                status.Add(Input.Up);
            if (keyState.IsKeyDown(Keys.Left) || padState.DPad.Left == ButtonState.Pressed || padState.ThumbSticks.Left.X == -1)
                status.Add(Input.Left);
            if (keyState.IsKeyDown(Keys.Down) || padState.DPad.Down == ButtonState.Pressed || padState.ThumbSticks.Left.Y == -1)
                status.Add(Input.Down);
            if (keyState.IsKeyDown(Keys.Right) || padState.DPad.Right == ButtonState.Pressed || padState.ThumbSticks.Left.X == 1)
                status.Add(Input.Right);
            if (keyState.IsKeyDown(Keys.Z) || padState.Buttons.X == ButtonState.Pressed)
                status.Add(Input.X);
            if (keyState.IsKeyDown(Keys.Enter) || padState.Buttons.Start == ButtonState.Pressed)
                status.Add(Input.Start);
            if (padState.Buttons.Back == ButtonState.Pressed || keyState.IsKeyDown(Keys.Back))
                status.Add(Input.Back);

        }
    }
}
