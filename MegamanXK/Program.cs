using System;

namespace MegamanXK
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (MegaMain game = new MegaMain())
            {
                game.Run();
            }
        }
    }
#endif
}

