﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK
{
    class GUI
    {
        Texture2D sheet;

        Rectangle healthbarBase;
        Rectangle healthbarMidOn;
        Rectangle healthbarMidOff;
        Rectangle healthbarTop;

        readonly Vector2 position;
        const int scale = 3;

        const int maxhealth = 20;
        int healthPoints = maxhealth;

        public GUI()
        {
            sheet = MegaMain.getContentManager().Load<Texture2D>("shots and items");
            healthbarTop = new Rectangle(2, 13, 14, 4);
            healthbarMidOff = new Rectangle(2, 19, 14, 2);
            healthbarMidOn = new Rectangle(2, 37, 14, 2);
            healthbarBase = new Rectangle(2, 55, 14, 16);

            position = new Vector2(50, 50);
        }

        public void Update(GameTime gameTime, int HP)
        {
            if (HP < 0)
                healthPoints = 0;
            else
                healthPoints = HP;
        }

        public void Draw(GameTime gameTime, SpriteBatch batch)
        {
            Rectangle mid = new Rectangle((int)position.X, (int)position.Y + scale * healthbarTop.Height, scale * healthbarMidOff.Width, scale * healthbarMidOff.Height);

            batch.Begin();
            batch.Draw(sheet, new Rectangle((int)position.X, (int)position.Y, healthbarTop.Width * scale, healthbarTop.Height * scale), healthbarTop, Color.White);

            for (int i = 0; i < maxhealth - healthPoints; i++)
            {
                batch.Draw(sheet, mid, healthbarMidOff, Color.White);
                mid.Offset(0, healthbarMidOff.Height * scale);
            }

            for (int i = 0; i < healthPoints; i++)
            {
                batch.Draw(sheet, mid, healthbarMidOn, Color.White);
                mid.Offset(0, healthbarMidOff.Height * scale);
            }

            batch.Draw(sheet, new Rectangle(mid.X, mid.Y, scale * healthbarBase.Width, scale * healthbarBase.Height), healthbarBase, Color.White);
            batch.End();
        }

    }
}
