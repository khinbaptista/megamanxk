using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MegamanXK
{
    /*
     
    TODO:
        > Test collisions
        > Spawn Megaman (use collisions to spawn)
        > Control screen roll
        > Add enemies
        > Control health points
        > Add victory (end) logic
     
    */

    public class MegaMain : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        static List<GameScreen> screens;
        static ContentManager contentManager;

        InputHandler inputHandler;


        // this enum is useless. OR IS IT?
        public enum GameState
        {
            Menu,
            Running,
            Paused,
            Wait    // no input is to be handled, but processing is running.
        }
        static GameState gameState { get; set; }

        public MegaMain()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            screens = new List<GameScreen>();

            contentManager = this.Content;

            gameState = GameState.Menu;
            inputHandler = new InputHandler();

            graphics.PreferredBackBufferWidth = 1500;
            graphics.PreferredBackBufferHeight = 762;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            screens.Add(new Screens.MenuScreen(GraphicsDevice.Viewport));

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            foreach (GameScreen screen in screens)
                screen.LoadContent();
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            inputHandler.Update();

            List<GameScreen> screensToUpdate = new List<GameScreen>();
            GameScreen screenUpdating;
            int index;

            foreach (GameScreen screen in screens)
                screensToUpdate.Add(screen);

            while(screensToUpdate.Count > 0)
            {
                index = screensToUpdate.Count - 1;
                screenUpdating = screensToUpdate[index];
                screensToUpdate.RemoveAt(index);

                
                screenUpdating.Update(gameTime);
                
                if (screenUpdating.screenState == ScreenState.Active && screenUpdating.onTop)// || screenUpdating.screenState == ScreenState.TransitionOn)
                    screenUpdating.HandleInput(inputHandler);
            }
            
            if (screens.Count == 0)
                this.Exit();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            foreach (GameScreen screen in screens)
                if (!(screen.screenState == ScreenState.Hidden))
                    screen.Draw(gameTime, spriteBatch);

            base.Draw(gameTime);
        }

        static public List<GameScreen> getScreens() { return screens; }

        static public ContentManager getContentManager() { return contentManager; }
    }
}
