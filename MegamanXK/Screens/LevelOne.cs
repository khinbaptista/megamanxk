﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Screens
{
    class LevelOne : GameScreen
    {
        Texture2D platform;

        Rectangle[] backWindowSource;
        Rectangle[] backWindowDestination;
        Rectangle frontWindowSource;

        private GUI userInterface = new GUI();
        private const int gravitySpeed = 7;

        int offsetX = 0;
        int offsetY = 0;

        Rectangle viewportRect;

        private List<Rectangle> platformCollider;
        private List<Rectangle> platformPits;
        private List<Actors.Projectile> lemons;

        private SoundEffect hitSFX;
        private Song soundtrack;

        /// <summary>
        /// Location of the rectangle for environment source. UNUSED
        /// </summary>
        Point location;

        Actors.Character megaman;
        List<Actors.Enemy> enemies;


        // debug
        Texture2D WHITE;

        public LevelOne(Viewport vp)
            : base(vp)
        {
            platform = MegaMain.getContentManager().Load<Texture2D>(@"LevelOne/flame mammoth stage");
            lemons = new List<Actors.Projectile>();

            // debug
            WHITE = MegaMain.getContentManager().Load<Texture2D>("blank");

            backWindowSource = new Rectangle[2];
            backWindowSource[0] = new Rectangle(6, 44, 598, 205);
            backWindowSource[1] = new Rectangle(2844, 813, 792, 349);

            backWindowDestination = new Rectangle[3];
            backWindowDestination[0] = new Rectangle(0, 0, 3192, viewport.Height);
            backWindowDestination[1] = new Rectangle(3192, 0, 3192, viewport.Height);
            backWindowDestination[2] = new Rectangle(2503, 0, 1914, viewport.Height);

            location = new Point(6, 264);
            frontWindowSource = new Rectangle(location.X, location.Y, 500, 254);

            megaman = new Actors.Character(this);

            transitionTime = TimeSpan.FromSeconds(0.4);

            viewportRect = new Rectangle(0, 0, viewport.Width, viewport.Height);
            SetupColliders();

            enemies = new List<Actors.Enemy>();
            enemies.Add(new Actors.Enemy(this, new Vector2(300, 50), true));
            enemies.Add(new Actors.Enemy(this, new Vector2(300, 50), false));
            enemies.Add(new Actors.Enemy(this, new Vector2(1500, 100), false, false));
            enemies.Add(new Actors.Enemy(this, new Vector2(1700, 100), false, false));
            enemies.Add(new Actors.Enemy(this, new Vector2(3000, 100), false, false));
            enemies.Add(new Actors.Enemy(this, new Vector2(4176, 228), false, false));
            enemies.Add(new Actors.Enemy(this, new Vector2(4482, 219), false, true));
            enemies.Add(new Actors.Enemy(this, new Vector2(5046, 357), false, false));
            enemies.Add(new Actors.Enemy(this, new Vector2(6225, 147), false, false));
            enemies.Add(new Actors.Enemy(this, new Vector2(7524, 1200), false, false));
            enemies.Add(new Actors.Enemy(this, new Vector2(9852, 774), false, false));


            hitSFX = MegaMain.getContentManager().Load<SoundEffect>(@"Sounds/lemonHit");
            soundtrack = MegaMain.getContentManager().Load<Song>(@"Sounds/stageST");

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(soundtrack);
        }

        private void SetupColliders()
        {
            platformCollider = new List<Rectangle>();

            platformCollider.Add(new Rectangle(0, 526, 818, 200));
            platformCollider.Add(new Rectangle(819, 381, 285, 300));
            platformCollider.Add(new Rectangle(1251, 382, 618, 96));
            platformCollider.Add(new Rectangle(2065, 382, 573, 96));
            platformCollider.Add(new Rectangle(2835, 382, 282, 375));
            platformCollider.Add(new Rectangle(3117, 576, 288, 165));
            platformCollider.Add(new Rectangle(3600, 477, 95, 195));

            // 31 steps of 12 pixels (4 * 3)
            for (int i = 0; i < 31; i++)
                platformCollider.Add(new Rectangle(3695 + 12 * i, 483 - 3 * i, 12, 96));

            platformCollider.Add(new Rectangle(4067, 388, 203, 96));
            platformCollider.Add(new Rectangle(4418, 388, 252, 96));

            for (int i = 0; i < 31; i++)
                platformCollider.Add(new Rectangle(4670 + 12 * i, 393 + 3 * i, 12, 96));

            platformCollider.Add(new Rectangle(5042, 485, 94, 192));
            platformCollider.Add(new Rectangle(5280, 576, 339, 189));
            platformCollider.Add(new Rectangle(5619, 477, 576, 975));
            platformCollider.Add(new Rectangle(6387, 477, 573, 381));
            platformCollider.Add(new Rectangle(6480, 39, 480, 438));
            platformCollider.Add(new Rectangle(6192, 1338, 1443, 150));
            platformCollider.Add(new Rectangle(7726, 1251, 1056, 87));
            platformCollider.Add(new Rectangle(8928, 1203, 912, 87));
            platformCollider.Add(new Rectangle(9381, 1041, 123, 21));
            platformCollider.Add(new Rectangle(9990, 1251, 804, 87));
            platformCollider.Add(new Rectangle(9654, 915, 372, 87));
            platformCollider.Add(new Rectangle(10233, 867, 657, 87));


            platformPits = new List<Rectangle>();

            platformPits.Add(new Rectangle(1104, 657, 1731, 115));
            platformPits.Add(new Rectangle(3408, 657, 1887, 115));
            platformPits.Add(new Rectangle(7632, 1404, 3240, 120));
        }

        public override void Update(GameTime gameTime)
        {
            if (onTop)  //(transitionState == 0) <- this causes some bug...
            {
                ApplyPhysics(megaman);
                megaman.Update(gameTime);

                if (megaman.getPosition().X > viewportRect.Right - 100 && megaman.getAction() == Actors.Action.Idle)
                    megaman.ChangeAction(Actors.Action.Victory);

                // update enemies and projectiles (lemons)
                foreach (Actors.Projectile lemon in lemons)
                    lemon.Update(gameTime);
                UpdateLemons();

                foreach (Actors.Enemy enemy in enemies)
                    enemy.Update(gameTime);
                UpdateEnemies();

            }

            userInterface.Update(gameTime, megaman.getHP());

            base.Update(gameTime);
        }

        private void UpdateEnemies()
        {
           // after this, check the lemon creation
            
            int i = 0;
            while (i < enemies.Count)
            {
                if (!enemies[i].isAwake())
                    if (enemies[i].getPosition().Intersects(viewportRect))
                        enemies[i].Wakeup();
                    else
                    {
                        i++;
                        continue;
                    }

                if (enemies[i].isAwake() && !enemies[i].getPosition().Intersects(viewportRect))
                {
                    enemies[i].GotHit(20);
                    i++;
                    continue;
                }

                if (enemies[i].isDead())
                {
                    enemies.RemoveAt(i);
                    //i--;
                    continue;
                }

                enemies[i].Move();
                enemies[i].setPosition(enemies[i].getPosition().X, enemies[i].getPosition().Y + gravitySpeed);

                foreach (Rectangle floor in platformCollider)
                {
                    if (enemies[i].getPosition().Intersects(floor))
                    {
                        if (enemies[i].getPosition().Bottom > floor.Top && enemies[i].getPosition().Top < floor.Top)
                            enemies[i].setPosition(enemies[i].getPosition().X, floor.Y - enemies[i].getPosition().Height);
                        else if (enemies[i].getPosition().Right > floor.Left && enemies[i].getPosition().Left < floor.Left)
                        {
                            enemies[i].setFlip(false);
                            enemies[i].setPosition(floor.Left - enemies[i].getPosition().Width, enemies[i].getPosition().Y);
                        }
                        else if (enemies[i].getPosition().Left < floor.Right && enemies[i].getPosition().Right > floor.Right)
                        {
                            enemies[i].setFlip(true);
                            enemies[i].setPosition(floor.Right, enemies[i].getPosition().Y);
                        }
                    }
                }

                i++;
            }
        }

        private void ApplyPhysics(Actors.Actor subject)
        {
            Rectangle subjectRect;// = subject.getPosition();
            bool intersected = false;

            if (megaman.isPreSpawning())
                return;

            subject.setPosition(subject.getPosition().X, subject.getPosition().Y + gravitySpeed);

            foreach (Rectangle floor in platformCollider)
            {
                subjectRect = subject.getPosition();

                if (subjectRect.Intersects(floor))
                {
                    intersected = true;
                                                       //50
                    if (subjectRect.Bottom - floor.Top < 25 && subjectRect.Top < floor.Top)
                    {
                        subject.setPosition(subjectRect.X, floor.Y - subjectRect.Height);
                        if (subject.getAction() == Actors.Action.Jumping && subject.getFrame() == 3)
                            subject.ChangeAction(Actors.Action.Falling);
                        else if (subject.getAction() == Actors.Action.JumpShoot && subject.getFrame() == 3)
                            subject.ChangeAction(Actors.Action.FallShoot);
                    }

                    else if (subjectRect.Right > floor.Left && subjectRect.Left < floor.Left)
                        subject.setPosition(floor.Left - subjectRect.Width, subjectRect.Y);

                    else if (subjectRect.Left < floor.Right && subjectRect.Right > floor.Right)
                        subject.setPosition(floor.Right, subjectRect.Y);

                    else if (subjectRect.Top < floor.Bottom && subjectRect.Bottom > floor.Bottom)
                        subject.setPosition(subjectRect.X, floor.Bottom);
                }
            }

            foreach (Rectangle pit in platformPits)
            {
                subjectRect = subject.getPosition();

                if (subjectRect.Intersects(pit))
                    if (subjectRect.Bottom - pit.Top < 50 && subjectRect.Top < pit.Top)
                    {
                        subject.setPosition(subjectRect.X, pit.Y - subjectRect.Height);

                        intersected = true;

                        if (subject.getAction() != Actors.Action.Hit && subject.getAction() != Actors.Action.Dying)
                            subject.gotHit(20);
                    }
            }

            if (!intersected && subject.getAction() != Actors.Action.Jumping && subject.getAction() != Actors.Action.Falling
                && subject.getAction() != Actors.Action.JumpShoot && subject.getAction() != Actors.Action.FallShoot)
                subject.ChangeAction(Actors.Action.Jumping, 3);

            if (!intersected && megaman.getPosition().Y + megaman.getPosition().Height > viewportRect.Height - megaman.getPosition().Height / 2)// - 30)
            {
                megaman.setPosition(subject.getPosition().X, subject.getPosition().Y - gravitySpeed);
                SlideY(gravitySpeed);
            }

            foreach (Actors.Enemy enemy in enemies)
                if (megaman.getPosition().Intersects(enemy.getPosition()) && megaman.getAction() != Actors.Action.Hit)
                {
                    megaman.gotHit(enemy.getDamage());
                    //enemy.toggleFlip();
                    enemy.Move();
                }
        }

        private void UpdateLemons()
        {
            int i = 0;
            while (i < lemons.Count)
            {
                lemons[i].UpdatePosition();

                if (lemons[i].GetRectangle().Intersects(megaman.GetPosition()))
                {
                    megaman.gotHit(lemons[i].GetDamage());
                    lemons.RemoveAt(i);
                    hitSFX.Play();
                    i--;
                }

                int j = 0;
                bool intersected = false;

                while (j < enemies.Count && !intersected)
                {
                    if (lemons[i].GetRectangle().Intersects(enemies[j].getPosition()))
                    {
                        intersected = true;
                        enemies[j].GotHit(lemons[i].GetDamage());
                        lemons.RemoveAt(i);
                        hitSFX.Play();
                        i--;
                    }
                    
                    j++;
                }

                i++;
            }

            // Look for collisions between lemons and platform colliders
            i = 0;
            while (i < lemons.Count)
            {
                bool intersected = false;

                int j = 0;
                while (!intersected && j < platformCollider.Count)
                {
                    if (lemons[i].GetRectangle().Intersects(platformCollider[j]))
                    {
                        intersected = true;
                        lemons.RemoveAt(i);
                        hitSFX.Play();
                        i--;
                    }
                    j++;
                }

                i++;
            }

        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            batch.Begin();

            batch.Draw(platform, backWindowDestination[0], backWindowSource[0], Color.White);
            batch.Draw(platform, backWindowDestination[1], backWindowSource[0], Color.White);
            batch.Draw(platform, backWindowDestination[2], backWindowSource[1], Color.White);
            batch.Draw(platform, viewportRect, frontWindowSource, Color.White);

            foreach (Actors.Projectile lemon in lemons)
                lemon.Draw(gameTime, batch, true);

            // debug
            //foreach (Rectangle floorPiece in platformCollider)
            //    batch.Draw(WHITE, floorPiece, new Color(0, 0, 0, 0.7f));
            //foreach (Rectangle lavaPiece in platformPits)
            //    batch.Draw(WHITE, lavaPiece, new Color(1, 0, 0, 0.3f));

            batch.End();

            megaman.Draw(gameTime, batch);

            foreach (Actors.Enemy enemy in enemies)
                enemy.Draw(gameTime, batch);

            userInterface.Draw(gameTime, batch);

            base.Draw(gameTime, batch);
        }

        public override void HandleInput(InputHandler input)
        {
            if (input.status.Contains(Input.Start))
            {
                onTop = false;
                MegaMain.getScreens().Add(new PauseScreen(viewport));
            }

            megaman.HandleInput(input);
        }

        public override Rectangle GetSpawnCollider() { return platformCollider[0]; }

        public override bool SlideX(int offset)
        {
            bool success;

            offsetX += offset;
            success = offsetX >= 0 && offsetX <= 3136;

            if (success)
            {
                backWindowDestination[0].Offset(-offset * 3, 0);
                backWindowDestination[1].Offset(-offset * 3, 0);
                backWindowDestination[2].Offset(-offset * 3, 0);

                frontWindowSource.Offset(offset, 0);

                for (int i = 0; i < platformCollider.Count; i++)
                    platformCollider[i] = new Rectangle(platformCollider[i].X - offset * 3, platformCollider[i].Y, platformCollider[i].Width, platformCollider[i].Height);
                for (int i = 0; i < platformPits.Count; i++)
                    platformPits[i] = new Rectangle(platformPits[i].X - offset * 3, platformPits[i].Y, platformPits[i].Width, platformPits[i].Height);

                foreach (Actors.Enemy enemy in enemies)
                    enemy.setPosition(enemy.getPosition().X - offset * 3, enemy.getPosition().Y);

            }
            else
                offsetX = (int)MathHelper.Clamp(offsetX, 0, 3136);

            return success;

        }

        public override bool SlideY(int offset)
        {
            bool success;

            if (megaman.getPosition().X + offsetX < 1929)
                return false;

            offsetY += offset;
            success = offsetY >= 0 && offsetY <= 768;

            if (success)
            {
                backWindowDestination[0].Offset(0, -offset / 3);
                backWindowDestination[1].Offset(0, -offset / 3);
                backWindowDestination[2].Offset(0, -offset / 3);

                frontWindowSource.Offset(0, offset);

                for (int i = 0; i < platformCollider.Count; i++)
                    platformCollider[i] = new Rectangle(platformCollider[i].X, platformCollider[i].Y - offset * 3, platformCollider[i].Width, platformCollider[i].Height);
                for (int i = 0; i < platformPits.Count; i++)
                    platformPits[i] = new Rectangle(platformPits[i].X, platformPits[i].Y - offset * 3, platformPits[i].Width, platformPits[i].Height);

                foreach (Actors.Enemy enemy in enemies)
                    enemy.setPosition(enemy.getPosition().X, enemy.getPosition().Y - offset * 3);
            }
            else
                offsetY = (int)MathHelper.Clamp(offsetY, 0, 759);

            return success;
        }

        public override void ManDown()
        {
            MediaPlayer.Stop();
            MegaMain.getScreens().Add(new Screens.GameOverScreen(viewport));
            ExitScreen();
        }

        public override void Victory()
        {
            MediaPlayer.Stop();
            MegaMain.getScreens().Add(new Screens.VictoryScreen(viewport));
            ExitScreen();
        }

        public override void NewProjectile(Actors.Projectile projectile) { lemons.Add(projectile); }
    }
}
