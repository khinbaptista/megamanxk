﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Screens
{
    class PauseScreen : GameScreen
    {
        //Texture2D logo;
        Texture2D text;

        public PauseScreen(Viewport vp)
            : base(vp)
        {
            transitionTime = TimeSpan.FromSeconds(0.5);
            LoadContent();
        }

        public override void LoadContent()
        {
            ContentManager content = MegaMain.getContentManager();

            //logo = content.Load<Texture2D>("mega_man_x_logo_final");
            text = content.Load<Texture2D>("gamePausedText");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void HandleInput(InputHandler input)
        {
            int creatorIndex = MegaMain.getScreens().IndexOf(this) - 1;     // index of the previous screen, which created this PauseScreen

            if (input.status.Contains(Input.Start))
            {
                MegaMain.getScreens()[creatorIndex].ComeBack();
                ExitScreen();
            }
            else if (input.status.Contains(Input.Back))
            {
                MegaMain.getScreens().RemoveAt(creatorIndex);
                MegaMain.getScreens().Add(new MenuScreen(viewport));
                ExitScreen();
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            batch.Begin();
            //batch.Draw(logo, new Vector2(494.0f, 80.0f), Color.White);
            batch.Draw(text, new Vector2(0, 0), Color.White); // 380 350
            batch.End();

            base.Draw(gameTime, batch);
        }
    }
}
