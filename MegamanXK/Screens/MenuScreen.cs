﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Screens
{
    class MenuScreen : GameScreen
    {
        ContentManager content;
        Texture2D megaman;
        
        Texture2D menu;
        bool start = true;
        Rectangle megamanPosition;

        private SoundEffect menuMoveSFX;
        private Song menuST;

        public MenuScreen(Viewport vp)
            : base(vp)
        {
            transitionTime = TimeSpan.FromSeconds(0.6);
            LoadContent();
        }

        public override void LoadContent()
        {
            if (content == null)
                content = MegaMain.getContentManager();

            //logo = content.Load<Texture2D>("mega_man_x_logo_final");
            menu = content.Load<Texture2D>("menu game start");
            megaman = content.Load<Texture2D>("megamanSheet02");
            menuMoveSFX = content.Load<SoundEffect>(@"Sounds/guiMenuMove");
            menuST = content.Load<Song>(@"Sounds/menuST");

            MediaPlayer.Play(menuST);
        }

        public override void UnloadContent()
        {
            content.Unload();
        }

        public override void Update(GameTime gameTime)
        {
            if (start)
                megamanPosition = new Rectangle(420, 410, 80, 120);
            else
                megamanPosition = new Rectangle(420, 500, 80, 120);
            
            base.Update(gameTime);
        }

        public override void HandleInput(InputHandler input)
        {
            if (input.status.Contains(Input.Up) && !start || input.status.Contains(Input.Down) && start)
            {
                start = !start;
                menuMoveSFX.Play();
            }

            if (input.status.Contains(Input.A) || input.status.Contains(Input.Start))
                if (start)
                {
                    MediaPlayer.Stop();
                    MegaMain.getScreens().Add(new LevelOne(viewport));
                    ExitScreen();
                }
                else
                {
                    MediaPlayer.Stop();
                    ExitScreen();
                }
                    
        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            batch.Begin();

            //batch.Draw(logo, new Vector2(494.0f, 80.0f), Color.White);
            batch.Draw(menu, new Vector2(0, 0), Color.White); // 528 525
            batch.Draw(megaman, megamanPosition, new Rectangle(0, 60, 40, 60), Color.White);

            batch.End();

            base.Draw(gameTime, batch);
        }
    }
}
