﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Screens
{
    class GameOverScreen : GameScreen
    {
        # region ATTRIBUTES

        ContentManager content;
        Texture2D message;

        #endregion

        #region METHODS

        public GameOverScreen(Viewport vp)
            : base(vp)
        {
            transitionTime = TimeSpan.FromSeconds(1.5);
            LoadContent();
        }

        public override void LoadContent()
        {
            if (content == null)
                content = MegaMain.getContentManager();

            message = content.Load<Texture2D>("gameoverbackground");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void HandleInput(InputHandler input)
        {
            bool exit = false;

            if (input.status.Contains(Input.A))
            {
                MegaMain.getScreens().Add(new Screens.LevelOne(viewport));
                exit = true;
            }
            else if (input.status.Contains(Input.B))
            {
                MegaMain.getScreens().Add(new Screens.MenuScreen(viewport));
                exit = true;
            }
            else if (input.status.Contains(Input.Back))
                exit = true;

            if (exit)
                ExitScreen();
        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            batch.Begin();
            batch.Draw(message, new Vector2(0, 0), Color.White);
            batch.End();

            base.Draw(gameTime, batch);
        }

        #endregion
    }
}
