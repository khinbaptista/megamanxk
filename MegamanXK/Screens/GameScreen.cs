﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK
{
    public enum ScreenState
    {
        TransitionOn,
        Active,
        TransitionOff,
        Hidden
    }

    public abstract class GameScreen
    {
            #region ATTRIBUTES

        protected Viewport viewport;
        Texture2D blank;

        /// <summary>
        /// The screen is just a popup. Previous screen doesn't need to fade out.
        /// </summary>
        public bool isPopup { get; protected set; }

        /// <summary>
        /// Time the screen takes to transition.
        /// </summary>
        public TimeSpan transitionTime { get; protected set; }

        /// <summary>
        /// 0: screen is fully active;
        /// 1: transitioned to nothing (to be deleted).
        /// </summary>
        public float transitionState { get; protected set; }

        /// <summary>
        /// Alpha value for transparency in relation to the transition state.
        /// 255: fully active;
        /// 0: transitioned to nothing.
        /// </summary>
        public byte transitionAlpha { get { return (byte)(255 - transitionState * 255); } }

        /// <summary>
        /// Current screen state.
        /// </summary>
        public ScreenState screenState { get; protected set; }

        /// <summary>
        /// Indicates whether the screen should be deleted when hidden.
        /// </summary>
        public bool isExiting { get; protected set; }

        /// <summary>
        /// Indicates whether the screen is active or coming to life.
        /// </summary>
        public bool isActive { get { return (screenState == ScreenState.Active || screenState == ScreenState.TransitionOn); } }

        public bool onTop { get; set; }


#endregion

            #region METHODS

        public GameScreen(Viewport vp)
        {
            isPopup = false;
            transitionTime = TimeSpan.Zero;
            transitionState = 1;
            screenState = ScreenState.TransitionOn;
            isExiting = false;
            viewport = vp;
            onTop = true;

            blank = MegaMain.getContentManager().Load<Texture2D>("blank");
        }

        public virtual void LoadContent() { }

        public virtual void UnloadContent() { }
        
        public virtual void Update(GameTime gameTime)
        {
            if (isExiting || !onTop)      // if the screen is going to die or to be covered, it must transition off.
            {
                screenState = ScreenState.TransitionOff;

                if (transitionState == 1)       // if the transition is finished...
                    if (!onTop)                   // the screen must either be deleted or hidden.
                        screenState = ScreenState.Hidden;
                    else
                        MegaMain.getScreens().Remove(this);
            }
            else    // Otherwise, transition on.
            {
                if (transitionState == 0)
                    screenState = ScreenState.Active;
                else
                    screenState = ScreenState.TransitionOn;
            }


            // Now calculate the transition (if required)
            float transitionDelta;

            // The transition delta is calculated according to the time (won't be affected by framerate)
            if (transitionTime == TimeSpan.Zero)
                transitionDelta = 1.0f;
            else
                transitionDelta = (float)(gameTime.ElapsedGameTime.Milliseconds / transitionTime.TotalMilliseconds);


            // if the screen is transitioning, update it. Otherwise, don't even bother.
            if (screenState == ScreenState.TransitionOn || screenState == ScreenState.TransitionOff)
            {
                if (screenState == ScreenState.TransitionOn)
                    transitionState -= transitionDelta;
                else if (screenState == ScreenState.TransitionOff)
                    transitionState += transitionDelta;

                transitionState = MathHelper.Clamp(transitionState, 0.0f, 1.0f);
            }
        }

        public virtual void HandleInput(InputHandler input) { }

        public virtual void Draw(GameTime gameTime, SpriteBatch batch)
        {
            if (this.screenState == ScreenState.TransitionOff || this.screenState == ScreenState.TransitionOn)
            {
                batch.Begin();
                batch.Draw(blank, new Rectangle(0, 0, viewport.Width, viewport.Height), new Color(0, 0, 0, (byte)(255 - transitionAlpha)));
                batch.End();
            }
        }

        /// <summary>
        /// Tells the screen to go away. Unlike ScreenManager.RemoveScreen, which
        /// instantly kills the screen, this method respects the transition timings
        /// and will give the screen a chance to gradually transition off.
        /// </summary>
        public void ExitScreen()
        {
            if (transitionTime == TimeSpan.Zero)
            {
                // If the screen has a zero transition time, remove it immediately.
                //screenController.RemoveScreen(this);
                MegaMain.getScreens().Remove(this);
            }
            else
            {
                // Otherwise flag that it should transition off and then exit.
                isExiting = true;
            }
        }

        public void ComeBack()
        {
            onTop = true;
        }

        public virtual Rectangle GetSpawnCollider() { return new Rectangle(); }

        public virtual bool SlideX(int offset) { return false; }

        public virtual bool SlideY(int offset) { return false; }

        public Rectangle GetViewportRectangle() { return new Rectangle(0, 0, viewport.Width, viewport.Height); }

        public Viewport getViewport() { return viewport; }

        public virtual void ManDown() { }

        public virtual void Victory() { }

        public virtual void NewProjectile(Actors.Projectile projectile) { }

#endregion


    }
}
