﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MegamanXK.Screens
{
    class VictoryScreen : GameScreen
    {
        Texture2D message;
        Song victoryST;

        public VictoryScreen(Viewport vp)
            : base(vp)
        {
            transitionTime = TimeSpan.FromSeconds(1);
            LoadContent();
        }

        public override void LoadContent()
        {
            ContentManager content = MegaMain.getContentManager();
            message = content.Load<Texture2D>("victoryScreen");
            victoryST = content.Load<Song>(@"Sounds/finale");

            MediaPlayer.IsRepeating = false;
            MediaPlayer.Play(victoryST);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void HandleInput(InputHandler input)
        {
            if (input.status.Count > 0 && input.previousStatus.Count == 0)
            {
                MediaPlayer.Stop();
                MegaMain.getScreens().Add(new MenuScreen(viewport));
                ExitScreen();
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch batch)
        {
            batch.Begin();
            batch.Draw(message, new Vector2(0, 0), Color.White);
            batch.End();

            base.Draw(gameTime, batch);
        }
    }
}
